package com.strikersoft.mvp_template;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Strikersoft on 4/16/18.
 */
public interface MVPBaseViewState {
  void saveState(@NonNull Bundle outState);
  void restoreState(@Nullable Bundle savedInstanceState);
}
