package com.strikersoft.mvp_template;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import javax.inject.Inject;

/**
 * Created by Strikersoft on 4/16/18.
 */
public abstract class MVPBaseActivity<V extends MVPBaseContract.View, S extends MVPBaseViewState, R extends MVPBaseContract.Router, P extends MVPBaseContract.Presenter<V, S, R>> extends
    AppCompatActivity implements MVPBaseContract.View {

  @Inject
  protected P presenter;

  @SuppressWarnings("unchecked")
  @CallSuper
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    MVPBaseViewModel<V, S, R, P> viewModel = ViewModelProviders.of(this).get(MVPBaseViewModel.class);
    boolean isPresenterCreated = false;
    if (viewModel.getPresenter() == null) {
      initInjections();
      viewModel.setPresenter(presenter);
      isPresenterCreated = true;
    }
    presenter = viewModel.getPresenter();
    if (savedInstanceState != null){
      presenter.restoreState(savedInstanceState);
    }
    if (isPresenterCreated) {
      presenter.onPresenterCreated();
    }
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    presenter.saveState(outState);
  }

  @CallSuper
  @Override
  protected void onDestroy() {
    super.onDestroy();
  }

  @Override protected void onStart() {
    super.onStart();
    presenter.attachView((V) this);
    presenter.attachRouter(initRouter());
  }

  @Override protected void onStop() {
    super.onStop();
    presenter.detachView();
    presenter.detachRouter();
  }

  protected abstract void initInjections();
  protected abstract R initRouter();
}
