package com.strikersoft.mvp_template;

import android.arch.lifecycle.ViewModel;

/**
 * Created by Strikersoft on 4/16/18.
 */
public final class MVPBaseViewModel<V extends MVPBaseContract.View, S extends MVPBaseViewState, R extends MVPBaseContract.Router, P extends MVPBaseContract.Presenter<V, S, R>> extends ViewModel {
  private P presenter;

  void setPresenter(P presenter) {
    if (this.presenter == null) {
      this.presenter = presenter;
    }
  }

  P getPresenter() {
    return this.presenter;
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    presenter.onPresenterDestroy();
    presenter = null;
  }
}
