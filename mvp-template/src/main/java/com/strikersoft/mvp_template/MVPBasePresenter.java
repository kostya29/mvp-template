package com.strikersoft.mvp_template;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Strikersoft on 4/16/18.
 */
public abstract class MVPBasePresenter<V extends MVPBaseContract.View, S extends MVPBaseViewState, R extends MVPBaseContract.Router> implements
    MVPBaseContract.Presenter<V, S, R> {
  private V view;
  private R router;
  private S viewState;
  protected CompositeDisposable presenterLiveCompositeDisposable = new CompositeDisposable();
  protected CompositeDisposable viewLiveCompositeDisposable = new CompositeDisposable();
  protected CompositeDisposable routerLiveCompositeDisposable = new CompositeDisposable();

  public MVPBasePresenter(S viewState) {
    this.viewState = viewState;
  }

  @Override final public V getView() {
    return view;
  }

  @Override final public R getRouter() {
    return router;
  }

  @Override final public S getViewState() {
    return viewState;
  }

  @CallSuper
  @Override public void attachView(V view) {
    this.view = view;
  }

  @CallSuper
  @Override public void detachView() {
    view = null;
    viewLiveCompositeDisposable.clear();
  }

  @Override final public boolean isViewAttached() {
    return view != null;
  }

  @CallSuper
  @Override public void attachRouter(R router) {
    this.router = router;
  }

  @CallSuper
  @Override public void detachRouter() {
    router = null;
    routerLiveCompositeDisposable.clear();
  }

  @Override final public boolean isRouterAttached() {
    return router != null;
  }

  @CallSuper
  @Override
  public void onPresenterDestroy() {
    presenterLiveCompositeDisposable.clear();
  }

  @CallSuper
  @Override
  public void onPresenterCreated() {
  }

  @Override
  public void saveState(@NonNull Bundle outState) {
    if (viewState != null){
      viewState.saveState(outState);
    }
  }

  @Override
  public void restoreState(@Nullable Bundle savedInstanceState) {
    if (viewState != null){
      viewState.restoreState(savedInstanceState);
    }
  }
}
