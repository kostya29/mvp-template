package com.strikersoft.mvp_template;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import javax.inject.Inject;

/**
 * Created by Strikersoft on 4/16/18.
 */
public abstract class MVPBaseFragment<V extends MVPBaseContract.View, S extends MVPBaseViewState, R extends MVPBaseContract.Router, P extends MVPBaseContract.Presenter<V, S, R>> extends
    Fragment implements MVPBaseContract.View {

  @Inject
  protected P presenter;

  @SuppressWarnings("unchecked")
  @CallSuper
  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    MVPBaseViewModel<V, S, R, P> viewModel = ViewModelProviders.of(this).get(MVPBaseViewModel.class);
    boolean isPresenterCreated = false;
    if (viewModel.getPresenter() == null) {
      initInjections();
      viewModel.setPresenter(presenter);
      isPresenterCreated = true;
    }
    presenter = viewModel.getPresenter();
    if (savedInstanceState != null){
      presenter.restoreState(savedInstanceState);
    }
    if (isPresenterCreated)
      presenter.onPresenterCreated();
  }

  @CallSuper
  @Override public void onStart() {
    super.onStart();
    presenter.attachView((V) this);
    presenter.attachRouter(initRouter());
  }

  @CallSuper
  @Override public void onStop() {
    super.onStop();
    presenter.detachView();
    presenter.detachRouter();
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    presenter.saveState(outState);
  }

  protected abstract void initInjections();
  protected abstract R initRouter();
}
