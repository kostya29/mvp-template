package com.strikersoft.mvp_template;

import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Created by Strikersoft on 5/16/18.
 */
public class MVPResourceManager {
  private Context context;

  public MVPResourceManager(Context context) {
    this.context = context;
  }

  public String getString(@StringRes int stringId) {
    return context.getString(stringId);
  }
}
