package com.strikersoft.mvp_template;

/**
 * Created by Strikersoft on 5/18/18.
 */
public abstract class MVPLoadBaseActivity<V extends MVPLoadBaseContract.View, S extends MVPBaseViewState, R extends MVPLoadBaseContract.Router, P extends MVPLoadBaseContract.Presenter<V, S, R>>
    extends MVPBaseActivity<V,S,R,P> implements MVPLoadBaseContract.View {
}

