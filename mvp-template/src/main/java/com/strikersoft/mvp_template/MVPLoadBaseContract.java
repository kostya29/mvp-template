package com.strikersoft.mvp_template;

/**
 * Created by Strikersoft on 5/18/18.
 */
public interface MVPLoadBaseContract {
  interface View extends MVPBaseContract.View {
    void showProgress(String message);
    void showProgress(String message, boolean cancelable);
    void hideProgress();
    void showError(String message);
  }

  interface Router extends MVPBaseContract.Router {

  }

  interface Presenter<V extends MVPBaseContract.View, S extends MVPBaseViewState, R extends MVPBaseContract.Router> extends MVPBaseContract.Presenter<V,S,R> {

  }
}
