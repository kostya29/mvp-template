package com.strikersoft.mvp_template;

import android.support.v4.app.FragmentActivity;
import java.lang.ref.WeakReference;

/**
 * Created by Strikersoft on 4/16/18.
 */
public class MVPBaseRouter implements MVPBaseContract.Router {
  protected WeakReference<FragmentActivity> activityWeakReference;

  public MVPBaseRouter(FragmentActivity activity) {
    this.activityWeakReference = new WeakReference<>(activity);
  }

  public FragmentActivity getActivity() {
    if (activityWeakReference == null)
      return null;
    return activityWeakReference.get();
  }
}
