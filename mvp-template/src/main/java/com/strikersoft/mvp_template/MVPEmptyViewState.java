package com.strikersoft.mvp_template;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Strikersoft on 4/16/18.
 */
public class MVPEmptyViewState implements MVPBaseViewState {
  @Override public void saveState(@NonNull Bundle outState) {

  }

  @Override public void restoreState(@Nullable Bundle savedInstanceState) {

  }
}
