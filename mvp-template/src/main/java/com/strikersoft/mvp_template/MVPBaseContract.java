package com.strikersoft.mvp_template;

/**
 * Created by Strikersoft on 4/16/18.
 */
public interface MVPBaseContract {
  interface Router {

  }

  interface View {

  }

  interface Presenter<V extends MVPBaseContract.View, S extends MVPBaseViewState, R extends MVPBaseContract.Router> extends MVPBaseViewState {
    void attachView(V view);

    void detachView();

    V getView();

    S getViewState();

    R getRouter();

    void attachRouter(R router);

    void detachRouter();

    boolean isViewAttached();

    boolean isRouterAttached();

    void onPresenterCreated();

    void onPresenterDestroy();
  }
}
